#include "Camera.hpp"

Camera::Camera() {
	position = glm::vec3(0, 10, 50);
	forward = glm::vec3(0, 0, -1);
	up = glm::vec3(0, 1, 0);
	right = glm::vec3(1, 0, 0);
}

Camera::~Camera() {}

// Rotate a Point/Vector around world OY (0, 1, 0) with a specific angle(radians)
glm::vec3 Camera::RotateOY(const glm::vec3 P, float radians)
{
	glm::vec3 R;
	R.x = P.x * cos(radians) - P.z * sin(radians);
	R.y = P.y;
	R.z = P.x * sin(radians) + P.z * cos(radians);
	return R;
}

inline glm::vec3 Camera::RotateOX(const glm::vec3 P, float radians)
{
	glm::vec3 R;
	R.y = P.y * cos(radians) - P.z * sin(radians);
	R.x = P.x;
	R.z = P.y * sin(radians) + P.z * cos(radians);
	return R;
}

inline glm::vec3 Camera::RotateOZ(const glm::vec3 P, float radians)
{
	glm::vec3 R;
	R.x = P.x * cos(radians) - P.y * sin(radians);
	R.z = P.z;
	R.y = P.x * sin(radians) + P.y * cos(radians);
	return R;
}

// Rotate the camera in FPS mode over the local OX axis
void Camera::rotateFPS_OX(float angle)
{
	forward = glm::normalize(RotateOX(glm::normalize(forward), angle));
	up = glm::normalize(glm::cross(right, forward));
}

void Camera::rotateFPS_OY(float angle)
{
	forward = glm::normalize(RotateOY(glm::normalize(forward), angle));
	right = glm::normalize(RotateOY(glm::normalize(right), angle));
	up = glm::normalize(glm::cross(right, forward));
}

void Camera::rotateFPS_OZ(float angle)
{
	right = glm::normalize(RotateOZ(glm::normalize(right), angle));
	up = glm::normalize(glm::cross(right, forward));
}

void Camera::translateRight(float tx) {
	position += tx * glm::normalize(glm::vec3(right.x, 0, right.y));
}

void Camera::translateUpward(float ty) {
	position += ty * glm::normalize(up);
}

void Camera::translateForward(float tz) {
	position += tz * glm::normalize(forward);
}

glm::mat4 Camera::getViewMatrix() {
	return glm::lookAt(position, position + forward, up);
}

void Camera::setPosition(glm::vec3 position) {
	this->position = position;
}

glm::vec3 Camera::getPosition() {
	return position;
}