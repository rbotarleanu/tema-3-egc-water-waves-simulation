/* Botarleanu Robert-Mihai 331CB 
 * Pornind de la scheletul laboratorului 7.
 * Definition file for the GameWindowHandler class.
 * This class handles all operations related to the window - it loads the meshes, 
 * sends the uniform variables and wave characteristics.
 */

#pragma once

#include <ctime>
#include <list>

// mesh loader
#include "lab_mesh_loader.hpp"

// geometry: drawSolidCube, drawWireTeapot...
#include "lab_geometry.hpp"

// shader loader
#include "lab_shader_loader.hpp"

// glut interface
#include "lab_glut.hpp"

// camera abstraction
#include "Camera.hpp"

// wave
#include "Wave.hpp"

class GameWindowHandler : public lab::glut::WindowListener{

private:
	const float PI = 3.14159265358979323846;
	const unsigned int material_shininess = 25.0f;
	const float material_kd = 15.5f;
	const float material_ks = 5.5f;
	const float camera_rotation_speed = 3.0f;				// the rotation speed of the camera
	const float light_movement_speed = 1.5f;				// the movement speed of the light
	int vertices_per_edge = 100;							// number of vertices on each edge of the water plane

	bool inFullScreen = false;	// is the window in full screen
	glm::mat4 model_matrix, view_matrix, projection_matrix;										
	unsigned int gl_program_shader_gouraud;														

	unsigned int mesh_vbo_sphere, mesh_ibo_sphere, mesh_vao_sphere, mesh_num_indices_sphere;	// opengl containers for the light source
	unsigned int mesh_vbo_water, mesh_ibo_water, mesh_vao_water, mesh_num_indices_water;		// opengl containers for he water

	glm::vec3 light_position;			// the position of the light source
	
	glm::vec3 water_color, sun_color;
	
	Camera camera;		// camera abstraction for the view matrix
	// converts hex color to vec3 [0,1] for ease of use
	glm::vec3 getColorFromHexadecimal(int hex);
	
	void createWaterMesh();							// creates the water
	clock_t program_start_time;						// clock start time
	clock_t last_tick;								// last tick
	float atenuation_dt;							// deltatime since last attenuation
	const float atenuation_factor = 0.01f;			// constant atenuation factor
	float crt_atenuation = 0.0f;					// current atenuation
	const float time_between_atenuations = 0.5f;	// time in seconds between 2 atenuations
public:

	//constructor .. e apelat cand e instantiata clasa
	GameWindowHandler(); 
	//destructor .. e apelat cand e distrusa clasa
	~GameWindowHandler();


	//--------------------------------------------------------------------------------------------
	//functii de cadru ---------------------------------------------------------------------------

	//functie chemata inainte de a incepe cadrul de desenare, o folosim ca sa updatam situatia scenei ( modelam/simulam scena)
	void notifyBeginFrame();
	//functia de afisare (lucram cu banda grafica)
	void notifyDisplayFrame();
	//functie chemata dupa ce am terminat cadrul de desenare (poate fi folosita pt modelare/simulare)
	void notifyEndFrame();
	//functei care e chemata cand se schimba dimensiunea ferestrei initiale
	void notifyReshape(int width, int height, int previos_width, int previous_height);


	//--------------------------------------------------------------------------------------------
	//functii de input output --------------------------------------------------------------------

	//tasta apasata
	void notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y);
	//tasta ridicata
	void notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y);
	//tasta speciala (up/down/F1/F2..) apasata
	void notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y);
	//tasta speciala ridicata
	void notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y);
	//drag cu mouse-ul
	void notifyMouseDrag(int mouse_x, int mouse_y);
	//am miscat mouseul (fara sa apas vreun buton)
	void notifyMouseMove(int mouse_x, int mouse_y);
	//am apasat pe un boton
	void notifyMouseClick(int button, int state, int mouse_x, int mouse_y);
	//scroll cu mouse-ul
	void notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y);
};
