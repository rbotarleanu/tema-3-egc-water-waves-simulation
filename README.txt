BOTARLEANU ROBERT-MIHAI 331CB 
TEMA 3 - Simularea apei

-------------------------------------------------------------------------------
CUPRINS
	1. Structura programului
	2. Iluminare
	3. Tipuri de unde
	4. Calculul pozitiei vertecsilor in functie de unde
	5. Calculul normalelor vertecsilor la plan in functie de unde
	6. Bonusuri
	7. Keybindings
-------------------------------------------------------------------------------	
	1. Structura programului
		Programul este impartit in mai multe clase, pornind de la scheletul 
	laboratorului 7(Iluminare Gourard): 
		- GameWindowHandler ce se ocupa de desenarea pe ecran, de transmiterea
	caracteristicilor uniforme la shader si de camera.
		- Camera ce ofera o abstractizare pentru calculul matricei de vizualizare
		- Wave ce ofera un model de a abstractiza caracteristicile functiilor de
	unda utilizate, fiind folosit in principal pentru a retine caracteristicile
	fiecarei unde si a le trimite la shader.
	
		Programul foloseste banda grafica programabila, prin intermediul a 2 
	shadere: fragment_shader si vertex_shader.
		vertex_shader se ocupa de calculul pozitiilor vertecsilor si a normalelor
	lor dupa aplicarea functiilor de unda;
		fragment_shader realizeaza iluminarea folosind modelul Phong.

	2. Iluminare
		Modelul de iluminare folosit este cel Phong, implementata in fragment_shader. In 
	acest sens, vertex shader va trimite catre fragment_shader pozitia globala 
	a fiecarui vertex si a normalei sale. De asemenea, de la CPU se vor trimite
	variabile uniforme cu caracteristicile de material ale obiectelor, pozitia
	sursei de lumina si a observatorului.
		Iluminarea cu modelul Phong este realizata prin calculul si combinarea a 3
	componente: ambientala, difuza si speculara.
		Lumina ambientala este considerata constanta pentru toata scena.
		Lumina difuza este calculata in functie de constanta de material kd si de 
	produsul scalar dintre vectorul format de sursa de lumina si de pozitia globala
	cunoscuta fragmentului si vectorul normalei trimis de catre vertex shader.
		Lumina speculara este calculata ca fiind egala cu produsul dintre caracteristica
	de material ks si exponentiala cu baza produsul scalar dintre normala globala si vectorul
	unitate L + V si exponentul caracteristica de material "Shininess".
		La final, dupa ce cele 3 componente au fost combinate, lumina este aplicata prin inmultire
	culorii primite de la vertex shader pentru mesh-ul curent.

	3. Tipuri de unde
		Algoritmul suporta 2 tipuri de functii de unda, primul descris in enuntul temei si cel 
	de-al doilea, pentru a oferi realism scenei prin intermediul unor valuri cu creste mai abrupte,
	de tip Gerstner.
		Ambele functii de unda suporta atat unde directionale cat si circulare, singura diferenta 
	fiind calculul vectorului directie explicit in cazul undelor circulare.
		
	4. Calculul pozitiei vertecsilor in functie de unde
		Algoritmul suporta pana la 3 functii de unda diferite ce pot fi aplicate pentru a obtine
	diferite tipuri de valuri.
		Pentru fieare vertex(in cadrul vertex shaderului) se realizeaza o compunere a undelor 
	prin suprapunere. Evident, pentru ca acest lucru sa fie posibil, este necesar ca valorile
	date pentru caracteristicile diferitelor unde sa fie compatibile(existand o relatie de
	interdependenta intre ele).
		In acest sens, fiecarui vertex ii sunt aplicate, pe rand, functiile de unda ale caror
	caracteristici sunt trimise de la CPU.

	5. Calculul normalelor vertecsilor la plan in functie de unde
		Dupa ce s-a determinat pozitia noua a vertexului curent dupa compunerea undelor, algoritmul
	recalculeaza normala la suprafata pentru vertexul curent astfel:
		I.	Determina cei 4 vecini in coordonate globale ale vertexului curent.
		II.	Aplica celor 4 vecini aceeasi succesiune de functii de unda care 
		au fost aplicate si vertexului curent.
		III. Cele 5 puncte(vertexul curent si cei 4 vecini) formeaza 4 triunghiuri, iar pentru fiecare
		se va calcula o normala ca fiind produsul dintre 2 laturi ale fiecarui triunghi.
		IV.	Se calculeaza media aritmetica a celor 4 normale, aceasta medie fiind noua normala folosita
		la calculul de iluminare.

	6. Bonusuri
	a)	Iluminarea folosita respecta modelul Phong, calculele fiind facute in cadrul fragment shaderului.
	b)	Algoritmul suporta atat unde calculate folosind formulele din enuntul temei, cat si folosind
	formulele Gerstner pentru a crea varfuri ale valurilor mai abrupte.
	c)  Undele se atenueaza in timp, incepand din momentul in care programul este rulat.
	
	7. Keybindings
		-> w/a/s/d translateaza camera
		-> W/A/S/D translateaza sursa de lumina
		-> 1/2 3/4 5/6 rotesc camera pe OX, OY, OZ
		-> t comuta pe modul wire de reprezentare a mesh-urilor
		