/* Header file for the camera class.
 * Pornind de la scheletul laboratorului 5.
 * Defines an abstraction for the camera. Used to compute the view matrix.
 */
#pragma once

//incarcator de meshe
#include "lab_mesh_loader.hpp"

//geometrie: drawSolidCube, drawWireTeapot...
#include "lab_geometry.hpp"

//incarcator de shadere
#include "lab_shader_loader.hpp"

//interfata cu glut, ne ofera fereastra, input, context opengl
#include "lab_glut.hpp"

class Camera {
private:
	glm::vec3 position;			// eye position
	glm::vec3 forward;			// forward vector
	glm::vec3 up;				// up vector
	glm::vec3 right;			// right vector
	// rotations on the 3 axes
	glm::vec3 RotateOX(const glm::vec3 P, float radians);
	glm::vec3 RotateOY(const glm::vec3 P, float radians);
	glm::vec3 RotateOZ(const glm::vec3 P, float radians);

public:
	Camera();
	~Camera();
	// translation of the camera
	void translateRight(float tx);
	void translateUpward(float ty);
	void translateForward(float tz);
	// camera FPS rotation for the 3 axes
	void rotateFPS_OX(float angle);
	void rotateFPS_OY(float angle);
	void rotateFPS_OZ(float angle);
	// compute the view matrix
	glm::mat4 getViewMatrix();
	void setPosition(glm::vec3 position);
	glm::vec3 getPosition();
};