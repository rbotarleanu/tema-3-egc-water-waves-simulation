/* Translation unit for the GameWindowHandler class. See the header file for more information. */
#pragma once

#include "GameWindowHandler.hpp"

GameWindowHandler::GameWindowHandler() {
		// get start time
		program_start_time = clock();

		//setari pentru desenare, clear color seteaza culoarea de clear pentru ecran (format R,G,B,A)
		glClearColor(0.5, 0.5, 0.5, 1);
		glClearDepth(1);			//clear depth si depth test (nu le studiem momentan, dar avem nevoie de ele!)
		glEnable(GL_DEPTH_TEST);	//sunt folosite pentru a determina obiectele cele mai apropiate de camera (la curs: algoritmul pictorului, algoritmul zbuffer)

		//incarca un shader din fisiere si gaseste locatiile matricilor relativ la programul creat
		gl_program_shader_gouraud = lab::loadShader("shadere\\vertex_shader.glsl", "shadere\\fragment_shader.glsl");
		
		//incarca o sfera ca sursa de lumina
		lab::loadObj("resurse\\sphere.obj", mesh_vao_sphere, mesh_vbo_sphere, mesh_ibo_sphere, mesh_num_indices_sphere);

		// incarca apa
		createWaterMesh();

		//lumina & material
		light_position = glm::vec3(-2, 7, -75);
		camera.setPosition(glm::vec3(10, 7, 0));

		//matrici de modelare si vizualizare
		model_matrix = glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

		//desenare wireframe
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		// set the colors
		water_color = getColorFromHexadecimal(0x101023);
		sun_color = getColorFromHexadecimal(0xFFFF33);

		// initialize deltatime for wave attenuation
		atenuation_dt = 0.0f;
}

GameWindowHandler::~GameWindowHandler() {
		//distruge shader
		glDeleteProgram(gl_program_shader_gouraud);

		// clears the light source
		glDeleteBuffers(1, &mesh_vbo_sphere);
		glDeleteBuffers(1, &mesh_ibo_sphere);
		glDeleteVertexArrays(1, &mesh_vao_sphere);

		// clears the water
		glDeleteBuffers(1, &mesh_vbo_water);
		glDeleteBuffers(1, &mesh_ibo_water);
		glDeleteVertexArrays(1, &mesh_vao_water);
}

void GameWindowHandler::createWaterMesh() {

	std::vector<lab::VertexFormat> vertices;
	std::vector<unsigned int> indices;

	// check for too many vertices 
	vertices_per_edge = glm::min(vertices_per_edge, 500);

	// the distance between the vertices
	float scale = 1.0f;

	// create vertices
	for (float z = 0; z > -vertices_per_edge; --z) {
		for (float x = 0; x < vertices_per_edge; ++x) {	// normal is on OY
			vertices.push_back(lab::VertexFormat(x * scale, 0, z * scale, 0, 1, 0));
		}
	}
	
	// create indices
	for (float z = 0; z < vertices_per_edge - 1; ++z) {
		for (float x = 0; x < vertices_per_edge - 1; ++x) {
			indices.push_back(z * vertices_per_edge + x);			//  |\  
			indices.push_back(z * vertices_per_edge + x + 1);		//  | \ 
			indices.push_back((z + 1) * vertices_per_edge + x);		//  |__\ 									
	
																	//   ____
			indices.push_back(z * vertices_per_edge + x + 1);		//   \  |
			indices.push_back((z + 1) * vertices_per_edge + x + 1);	//	  \ |
			indices.push_back((z + 1) * vertices_per_edge + x);		//     \|
		}
	}

	//vertex array object -> un obiect ce reprezinta un container pentru starea de desenare
	glGenVertexArrays(1, &mesh_vao_water);
	glBindVertexArray(mesh_vao_water);

	//vertex buffer object -> un obiect in care tinem vertecsii
	glGenBuffers(1, &mesh_vbo_water);
	glBindBuffer(GL_ARRAY_BUFFER, mesh_vbo_water);
	glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(lab::VertexFormat), &vertices[0], GL_STATIC_DRAW);

	//index buffer object -> un obiect in care tinem indecsii
	glGenBuffers(1, &mesh_ibo_water);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh_ibo_water);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	//legatura intre atributele vertecsilor si pipeline, datele noastre sunt INTERLEAVED.
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*)0);						//trimite pozitii pe pipe 0
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*)(sizeof(float)* 3));		//trimite normale pe pipe 1
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(lab::VertexFormat), (void*)(2 * sizeof(float)* 3));	//trimite texcoords pe pipe 2

	mesh_num_indices_water = indices.size();
}

glm::vec3
GameWindowHandler::getColorFromHexadecimal(int hex) {
	return glm::vec3(
		((hex >> 16) & 0xff) / 255.0f,		// R
		((hex >> 8) & 0xff) / 255.0f,		// G
		((hex) & 0xff) / 255.0f				// B
		);
}

void GameWindowHandler::notifyBeginFrame(void) {
}

void GameWindowHandler::notifyDisplayFrame(void) {
		//bufferele din framebuffer sunt aduse la valorile initiale (setate de clear color si cleardepth)
		//adica se sterge ecranul si se pune culoare (si alte propietati) initiala
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//foloseste shaderul
		glUseProgram(gl_program_shader_gouraud);

		glm::vec3 eye_position = camera.getPosition();

		clock_t crt_tick = clock();	// current CPU clock time
		float time = (crt_tick - program_start_time) / (float)CLOCKS_PER_SEC;	// time in seconds since program start
		atenuation_dt += (crt_tick - last_tick) / (float)CLOCKS_PER_SEC;		// time since last atenuation
		if (atenuation_dt > time_between_atenuations) { 
			// enough seconds have passed since last atenuation
			crt_atenuation += atenuation_factor;	// increase the wave atenuatino
			atenuation_dt = 0.0f;					// reset the timer
		}
		last_tick = crt_tick;	// update last tick

		//trimite variabile uniforme la shader
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_gouraud, "model_matrix"), 1, false, glm::value_ptr(model_matrix));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_gouraud, "view_matrix"), 1, false, glm::value_ptr(camera.getViewMatrix()));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_gouraud, "projection_matrix"), 1, false, glm::value_ptr(projection_matrix));
		glUniform3f(glGetUniformLocation(gl_program_shader_gouraud, "light_position"), light_position.x, light_position.y, light_position.z);
		glUniform3f(glGetUniformLocation(gl_program_shader_gouraud, "eye_position"), eye_position.x, eye_position.y, eye_position.z);
		glUniform1i(glGetUniformLocation(gl_program_shader_gouraud, "material_shininess"), material_shininess);
		glUniform1f(glGetUniformLocation(gl_program_shader_gouraud, "material_kd"), material_kd);
		glUniform1f(glGetUniformLocation(gl_program_shader_gouraud, "material_ks"), material_ks);
		
		//pune o sfera la pozitia luminii
		glm::mat4 matrice_translatie = glm::translate(model_matrix, glm::vec3(light_position.x, light_position.y, light_position.z));
		glm::mat4 matrice_scalare = glm::scale(model_matrix, glm::vec3(0.1, 0.1, 0.1));
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_gouraud, "model_matrix"), 1, false, glm::value_ptr(matrice_translatie * matrice_scalare));
		glUniform3f(glGetUniformLocation(gl_program_shader_gouraud, "in_color"), sun_color.r, sun_color.g, sun_color.b);
		glUniform1f(glGetUniformLocation(gl_program_shader_gouraud, "time"), time);	// sends the time
		glUniform1f(glGetUniformLocation(gl_program_shader_gouraud, "is_light_source"), 1.0f);	// this is the light source
		glBindVertexArray(mesh_vao_sphere);
		glDrawElements(GL_TRIANGLES, mesh_num_indices_sphere, GL_UNSIGNED_INT, 0);

		// the water
		glUniformMatrix4fv(glGetUniformLocation(gl_program_shader_gouraud, "model_matrix"), 1, false, glm::value_ptr(glm::mat4()));
		glUniform3f(glGetUniformLocation(gl_program_shader_gouraud, "in_color"), water_color.r, water_color.g, water_color.b);
		glUniform1f(glGetUniformLocation(gl_program_shader_gouraud, "time"), time);	// sends the time
		glUniform1f(glGetUniformLocation(gl_program_shader_gouraud, "is_light_source"), 0.0f); // this is not the light sources
		
		// creates a circular wave and sends its characteristics to the shader
		Wave circular_wave = Wave(1.5f - crt_atenuation, 1.0f, 2.0f, glm::vec3(50, 1000, -50), 0.5f);
		circular_wave.notifyShader(gl_program_shader_gouraud, 1);
	
		// creates a directional wave and sends its characteristics to the shader
		Wave dir_wave = Wave(1.3f - crt_atenuation, 1.0f, glm::vec3(0.5f, 0, -1), 10.0f, 0.5f);
		dir_wave.notifyShader(gl_program_shader_gouraud, 3);

		// creates a directional wave and sends its characteristics to the shader
		Wave gerstner_wave = Wave(1.0f - crt_atenuation, 1.0f, glm::vec3(-1.0f, 0, 0.5f), 5.0f, 0.5f);
		gerstner_wave.notifyShader(gl_program_shader_gouraud, 2);
		glBindVertexArray(mesh_vao_water);
		glDrawElements(GL_TRIANGLES, mesh_num_indices_water, GL_UNSIGNED_INT, 0);	
}

void 
GameWindowHandler::notifyReshape(int width, int height, int previos_width, int previous_height)
{
	//reshape
	if (height == 0) height = 1;
	glViewport(0, 0, width, height);
	projection_matrix = glm::perspective(90.0f, (float)width / (float)height, 0.1f, 10000.0f);
}

void GameWindowHandler::notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y)
{
	static bool wire = true;
	switch (key_pressed) {
		case 't':		wire = !wire;			// switch wire
						glPolygonMode(GL_FRONT_AND_BACK, (wire ? GL_LINE : GL_FILL));
																						break;
		case 27:		glutLeaveMainLoop();											break;  // exit
		case 32:		glDeleteProgram(gl_program_shader_gouraud);	// reload shader
						gl_program_shader_gouraud = 
							lab::loadShader("shadere\\shader_gouraud_vertex.glsl", 
							"shadere\\shader_gouraud_fragment.glsl");
																						break;
		case 'w': 		camera.translateForward(light_movement_speed);					break;
		case 'a':		camera.translateRight(-light_movement_speed);					break;
		case 's':		camera.translateForward(-light_movement_speed);					break;
		case 'd':		camera.translateRight(light_movement_speed);					break;
		case 'q': 		camera.translateUpward(-light_movement_speed);					break;
		case 'e':		camera.translateUpward(light_movement_speed);					break;
		case '1':		camera.rotateFPS_OY(-camera_rotation_speed * PI / 180);			break;
		case '2':		camera.rotateFPS_OY(camera_rotation_speed * PI / 180);			break;
		case '3':		camera.rotateFPS_OX(-camera_rotation_speed * PI / 180);			break;
		case '4':		camera.rotateFPS_OX(camera_rotation_speed * PI / 180);			break;
		case '5':		camera.rotateFPS_OZ(-camera_rotation_speed * PI / 180);			break;
		case '6':		camera.rotateFPS_OZ(camera_rotation_speed* PI / 180);			break;
		case 'W':		light_position += glm::vec3(0, 0, -light_movement_speed);		break;
		case 'A':		light_position += glm::vec3(-light_movement_speed, 0, 0);		break;
		case 'S':		light_position += glm::vec3(0, 0, light_movement_speed);		break;
		case 'D':		light_position += glm::vec3(light_movement_speed, 0, 0);		break;
		case 'Q':		light_position += glm::vec3(0, -light_movement_speed, 0);		break;
		case 'E':		light_position += glm::vec3(0, light_movement_speed, 0);		break;
	}
}

void GameWindowHandler::notifyEndFrame(){}

void GameWindowHandler::notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y) {}

void GameWindowHandler::notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y)
{
	if (key_pressed == GLUT_KEY_F1 && !inFullScreen) {
		glutFullScreenToggle();
		inFullScreen |= 1;
	}
	if (key_pressed == GLUT_KEY_F2 && inFullScreen) {
		glutFullScreenToggle();
		inFullScreen &= 0;
	}
}

//tasta speciala ridicata
void GameWindowHandler::notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y) {}
//drag cu mouse-ul
void GameWindowHandler::notifyMouseDrag(int mouse_x, int mouse_y) {}
//am miscat mouseul (fara sa apas vreun buton)
void GameWindowHandler::notifyMouseMove(int mouse_x, int mouse_y) {}

void GameWindowHandler::notifyMouseClick(int button, int state, int mouse_x, int mouse_y){ }

void GameWindowHandler::notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y) {
	std::cout << "Mouse scroll" << std::endl; 
}
