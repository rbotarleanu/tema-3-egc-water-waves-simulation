#version 330
layout(location = 0) out vec4 out_color;

// Botarleanu Robert-Mihai
// Pornind de la scheletul laboratorului 8.

in vec3 color;

uniform vec3 light_position;
uniform vec3 eye_position;
uniform int material_shininess;
uniform float material_kd;
uniform float material_ks;

in vec3 world_pos;
in vec3 world_normal;

float compute_phong_lighting() {
	float diffuse, specular, ambiental;

	// the light vector between the light position and the vertex
	vec3 L = normalize(light_position - world_pos);
	float nL = max(0.0, dot(world_normal, L));

	// compute the diffuse light with the material value given and the n.L value 
	diffuse = material_kd * nL;

	// the ambiental light is constant
	ambiental = 0.70f;

	// compute the specular light
	float specularPow = 0.0f;
	vec3 V = normalize(eye_position - world_pos);
	vec3 H = normalize(L + V);
	float nH = max(0.0, dot(world_normal, H));
	specularPow = pow(nH, material_shininess);
	
	// the specular light is equal to the specular power multiplied by the material characteristic
	specular = specularPow * material_ks;

	// combine the 3 components to get the Phong lighting
	return (diffuse + specular) + ambiental;
}

void main(){

	// compute the Phong lighting
	float light = compute_phong_lighting();
	
	// apply lighting to color and send it further on the pipe
	out_color = vec4(color * light, 1);
}